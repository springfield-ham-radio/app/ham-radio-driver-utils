## [3.0.0](https://gitlab.com/springfield-ham-radio/app/ham-radio-driver-utils/compare/v2.0.1...v3.0.0) (2024-12-02)


### ⚠ BREAKING CHANGES

* switch to ESM

### Features

* switch to ESM ([1bdcb42](https://gitlab.com/springfield-ham-radio/app/ham-radio-driver-utils/commit/1bdcb4259c183e479e54cb18965b4858d54b4fd8))

## [2.0.1](https://gitlab.com/springfield-ham-radio/app/ham-radio-driver-utils/compare/v2.0.0...v2.0.1) (2024-12-02)


### Bug Fixes

* display memory addresses in hex ([49e33db](https://gitlab.com/springfield-ham-radio/app/ham-radio-driver-utils/commit/49e33db7ca9602d2189fa4c9a1c753b114f9e4f8))

## [2.0.0](https://gitlab.com/springfield-ham-radio/app/ham-radio-driver-utils/compare/v1.1.0...v2.0.0) (2024-12-02)


### ⚠ BREAKING CHANGES

* switch to loglayer

### Features

* switch to loglayer ([31a9ee3](https://gitlab.com/springfield-ham-radio/app/ham-radio-driver-utils/commit/31a9ee36ca713f0edef894db3175bc3a8edac755))

## [1.1.0](https://gitlab.com/springfield-ham-radio/app/ham-radio-driver-utils/compare/v1.0.1...v1.1.0) (2024-12-01)


### Features

* add port to SegmentedMemoryDriver abstract functions ([b0cf810](https://gitlab.com/springfield-ham-radio/app/ham-radio-driver-utils/commit/b0cf810196f89786161017f0eb8705da8c8636f9))

## [1.0.1](https://gitlab.com/springfield-ham-radio/app/ham-radio-driver-utils/compare/v1.0.0...v1.0.1) (2024-12-01)


### Bug Fixes

* export missing code ([b75f3eb](https://gitlab.com/springfield-ham-radio/app/ham-radio-driver-utils/commit/b75f3eb603f2d8c4888bd92bbc2ca51c5942ba98))

## 1.0.0 (2024-12-01)


### Features

* add segmented memory ([d2f1ed3](https://gitlab.com/springfield-ham-radio/app/ham-radio-driver-utils/commit/d2f1ed30f66978b9f06a0e902c750036702123d1))
