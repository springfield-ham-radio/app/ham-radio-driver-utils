import { SerialPort } from 'serialport';
import { LogLayer } from 'loglayer';

import { RadioDriver, RadioMemory, RadioProgram, RadioProgressIndicator, RadioCodec } from '@springfield/ham-radio-api';
import { RadioMemorySegment } from './memory-segment.js';
import { RadioSegmentedMemory } from './segmented-memory.js';

export abstract class RadioSegmentedMemoryDriver implements RadioDriver {
  private radioModelId: string;
  private codec: RadioCodec;
  protected logger: LogLayer;

  constructor(radioModelId: string, codec: RadioCodec, logger: LogLayer) {
    this.radioModelId = radioModelId;
    this.codec = codec;
    this.logger = logger;
  }

  public async readRadio(serialPortPath: string, progressIndicator: RadioProgressIndicator): Promise<RadioMemory | undefined> {
    let port;

    try {
      port = await this.connectToRadio(serialPortPath);

      progressIndicator.setValue(0);
      const memory = await this.readRadioMemory(port, progressIndicator);
      progressIndicator.setValue(1);

      return progressIndicator.isCanceled ? undefined : { radioModelId: this.radioModelId, contents: memory };
    } finally {
      if (port && port.isOpen) {
        this.logger.withMetadata({ serialPortPath }).debug('Closing serial port');
        port.close();
      }
    }
  }

  public async writeRadio(serialPortPath: string, memory: RadioMemory, progressIndicator: RadioProgressIndicator): Promise<void> {
    let port;

    try {
      port = await this.connectToRadio(serialPortPath);
      progressIndicator.setValue(0);
      await this.writeRadioMemory(port, memory.contents, progressIndicator);
      progressIndicator.setValue(1);
    } finally {
      if (port && port.isOpen) {
        this.logger.withMetadata({ serialPortPath }).debug('Closing serial port');
        port.close();
      }
    }
  }

  public decodeMemory(memory: RadioMemory): RadioProgram {
    return this.codec.decode(memory);
  }
  public encodeProgram(program: RadioProgram, memory: RadioMemory): RadioMemory {
    return this.codec.encode(program, memory);
  }

  protected async readRadioMemory(port: SerialPort, progressIndicator: RadioProgressIndicator): Promise<RadioSegmentedMemory> {
    this.logger.debug('Starging to read radio memory');
    const memory = new RadioSegmentedMemory();
    let progress = 0;

    try {
      await this.beginRadioRead(port);
      progress++;
      progressIndicator.setValue(progress / (this.getNumberMemorySegments() + 2));

      while (!progressIndicator.isCanceled) {
        const segment = await this.readSegment(port);

        if (segment === null) {
          break;
        }

        this.logger.withContext({ address: segment.startAddress.toString(16) }).debug('Read memory segment');
        memory.addSegment(segment);
        progress++;
        progressIndicator.setValue(progress / (this.getNumberMemorySegments() + 2));
      }
    } finally {
      try {
        this.logger.debug('Finished reading radio memory');
        await this.endRadioRead(port);
      } finally {
        progressIndicator.setValue(1);
      }
    }

    return memory;
  }

  protected async writeRadioMemory(port: SerialPort, memory: RadioSegmentedMemory, progressIndicator: RadioProgressIndicator): Promise<void> {
    this.logger.debug('Starging to write radio memory');
    let progress = 0;

    try {
      await this.beginRadioWrite(port);
      progress++;
      progressIndicator.setValue(progress / (this.getNumberMemorySegments() + 2));

      for (const segment of memory.getSegments()) {
        if (progressIndicator.isCanceled) {
          break;
        }

        progress++;
        this.logger.withMetadata({ address: segment.startAddress.toString(16) }).debug('Writing memory segment');
        await this.writeSegment(port, segment);
        progressIndicator.setValue(progress / (this.getNumberMemorySegments() + 2));
      }
    } finally {
      try {
        this.logger.debug('Finished writing radio memory');
        await this.endRadioWrite(port);
      } finally {
        progressIndicator.setValue(1);
      }
    }
  }

  protected abstract getNumberMemorySegments(): number;

  protected abstract connectToRadio(path: string): Promise<SerialPort>;

  protected abstract beginRadioRead(port: SerialPort): Promise<void>;

  protected abstract endRadioRead(port: SerialPort): Promise<void>;

  protected abstract beginRadioWrite(port: SerialPort): Promise<void>;

  protected abstract endRadioWrite(port: SerialPort): Promise<void>;

  protected abstract readSegment(port: SerialPort): Promise<RadioMemorySegment | null>;

  protected abstract writeSegment(port: SerialPort, segment: RadioMemorySegment): Promise<void>;
}
