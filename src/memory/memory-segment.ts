export interface RadioMemorySegment {
  startAddress: number;
  length: number;
  data: Uint8Array;
}
